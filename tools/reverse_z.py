import sys
from asmbox.gbb import Gbb
from asmbox.mdio import write_lammps_data

system = Gbb()

box = system.load_lammps_data(sys.argv[1])

for i, j in enumerate(system.types):
    if j < 3 or j > 11:
        system.xyz[i, 2] = system.xyz[i, 2] * -1

write_lammps_data(sys.argv[2], system, box)

