import sys
from asmbox.gbb import Gbb
from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
import copy

system = Gbb()

system, box = read_lammps_data_file(sys.argv[1])

write_lammps_data_file(sys.argv[2], system, box)