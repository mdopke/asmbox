#!/usr/bin/env

import numpy as np
import copy
import sys
from asmbox.functions import silanol_classification

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
import ast

if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    area = float(sys.argv[5])

    system, box = read_lammps_data_file(infile)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    atoms.assign_indices(system)

    isolated, vicinal, geminal = silanol_classification(system, atoms)
    total = len(isolated) + len(vicinal) + len(geminal)
    print('Isolated: %f' %(len(isolated)/total))
    print('Vicinal: %f' % (len(vicinal) / total))
    print('Geminal: %f' % (len(geminal) / total))
    print('Total: %f' % (total/float(area)/0.01))


