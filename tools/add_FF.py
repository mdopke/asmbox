#!/usr/bin/env

import numpy as np
import copy
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.asm import add_ion
from asmbox.FF import FF



if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    frc = []
    for ff in range(len(sys.argv) - 6):
        frc.append(sys.argv[ff+6])


    system, box = read_lammps_data_file(infile)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    FF.combine(frc)
    system = FF.apply(system)

    if 'clayFF' in frc:

        print('Removing bonds not necessary for clayFF')

        remove = []
        for i, j in enumerate(system.bonds):
            if j[0] not in [1, 3]:
                remove.append(i)

        for i in reversed(remove):
            system.bonds = np.delete(system.bonds, i, 0)

        remove = []
        for i, j in enumerate(system.angles):
            if j[0] not in [1, 4]:
                remove.append(i)

        for i in reversed(remove):
            system.angles = np.delete(system.angles, i, 0)


    write_lammps_data_file(outfile, system, box)

