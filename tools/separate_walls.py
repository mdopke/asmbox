#!/usr/bin/env

import numpy as np
import copy
import sys

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF
import ast



if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    distance = sys.argv[6]
    axis = sys.argv[7]


    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    d = float(distance)
    axis = int(axis)

    affected1 = []
    affected2 = []
    for i, j in enumerate(system.xyz):
        if j[axis] > 0:
            affected1.append(list(j))
        elif j[axis] < 0:
            affected2.append(list(j))

    max_ = np.min(np.asarray(affected1)[:, axis])
    min_ = np.max(np.asarray(affected2)[:, axis])

    diff = max_ - min_
    d -= diff

    for i, j in enumerate(system.xyz):
        if j[axis] > 0:
            system.xyz[i, axis] += d / 2
        elif j[axis] < 0:
            system.xyz[i, axis] -= d / 2

    box.lengths[axis] += d
    box.maxs[axis] += d/2
    box.mins[axis] -= d/2

    # FF.combine(['interface', 'tip4p2005'])
    # system = FF.apply(system)

    write_lammps_data_file(outfile, system, box, FF=False)

