#!/usr/bin/env

import numpy as np
import copy
import sys
import math

from asmbox.mdio2 import write_lammps_data_file
from asmbox.indices import Indices
from asmbox.box import Box
from asmbox.gbb import Gbb
from asmbox.asm import read_h2o
from asmbox.functions import insert_atom
from asmbox.FF import FF



def add_water(atoms, region, density):
    '''
    Function to add water in a cubic region

    :param fileh2o:
    :param system:
    :param box:
    :param atoms:
    :param region:
    :param density: in g/cc
    :return:
    '''
    # make such that I define a region and what density I expect within that

    system = Gbb()

    gmol = 18.01528
    NAvogadros = 6.022e23
    rho = density * 1e-24

    mins = np.zeros(3)
    maxs = np.zeros(3)
    lengths = np.zeros(3)

    for i in range(3):
        mins[i] = region[i, 0]
        maxs[i] = region[i, 1]
        lengths[i] = maxs[i] - mins[i]

    water_box = Box(lengths=lengths)

    print('Water box:')
    print(water_box)

    # check if any atom is inside of water box.
    for xyz in system.xyz:
        if xyz[0] < water_box.maxs[0] and xyz[0] > water_box.mins[0] and \
                xyz[1] < water_box.maxs[1] and xyz[1] > water_box.mins[1] and \
                xyz[2] < water_box.maxs[2] and xyz[2] > water_box.mins[2]:
            print('Region is not empty')
            exit()


    vol = water_box.lengths[0] * water_box.lengths[1] * water_box.lengths[2]

    water = math.ceil(rho * vol / gmol * NAvogadros)

    pts = []
    fac = 10
    while len(pts) < water:
        x = np.linspace(water_box.mins[0], water_box.maxs[0] - 2, int((water_box.lengths[0] - 2) / fac))
        y = np.linspace(water_box.mins[1], water_box.maxs[1] - 2, int((water_box.lengths[1] - 2) / fac))
        z = np.linspace(water_box.mins[2], water_box.maxs[2] - 2, int((water_box.lengths[2] - 2) / fac))[::-1]

        pts = np.zeros((int(len(x)*len(y)*len(z)), 3))
        # print(len(pts), fac, water)
        fac -= 0.25

    c = 0
    for i in range(len(z)):
        for j in range(len(y)):
            for k in range(len(x)):
                pts[c, 0] = x[k]
                pts[c, 1] = y[j]
                pts[c, 2] = z[i]
                c += 1

    np.random.shuffle(pts)

    print('Adding %i water molecules' % water)

    for path in sys.path:
        if path.find('asmbox') > -1:
            app = path[:path.find('asmbox')+6]
    h2o = read_h2o(app+'/h2o.txt')


    xyz = h2o.xyz
    c = 0
    resid = 0
    for i in range(0, water * 3, 3):
        c+=1
        resid += 1
        print('Adding water %5d / %5d\r' % ((i+1) / 3, water), end='')
        pos = pts[c-1, :]

        new_id = len(system.types)

        if c == 1:

            system.xyz = xyz + pos

            system.types = np.array([1, 2, 2])
            system.charges = np.array([h2o.charges[0][0], h2o.charges[1][0], h2o.charges[2][0]])
            system.resids = np.array([1, 1, 1])
            system.bonds = np.array([[1, 1, 2],
                                     [1, 1, 3]])

        else:

            system = insert_atom(system, xyz[0, :] + pos, 1, h2o.charges[0][0], c, 1, [new_id+1, new_id+2])
            system = insert_atom(system, xyz[1, :] + pos, 2, h2o.charges[1][0], c, 1, [])
            system = insert_atom(system, xyz[2, :] + pos, 2, h2o.charges[2][0], c, 1, [])

        atoms.indices['Ow'].append(new_id)
        atoms.indices['Hw'].append(new_id+1)
        atoms.indices['Hw'].append(new_id+2)

    print('')


    return system, water_box, atoms


if __name__ == '__main__':

    # system.wrap(box)

    atom_types = {'Ow': 1, 'Hw': 2, 'Na':3, 'Cl':4}
    bond_types = {'OwHw': 1}
    angle_types = {'HwOwHw': 1}

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)


    cube = np.array([[float(sys.argv[3]), float(sys.argv[4])],
                     [float(sys.argv[5]), float(sys.argv[6])],
                     [float(sys.argv[7]), float(sys.argv[8])]])

    system, box, atoms = add_water(atoms, cube, 1)

    system = atoms.update(system)


    # system.bond_types = {}
    # system.bond_types[1] = atoms.bond_types['OwHw'][1:]
    #
    # system.angle_types = {}
    # system.angle_types[1] = atoms.angle_types['HwOwHw'][1:]

    # print('!!!!!!!!!!!!!!!ERROR!!!!!!!!!!!!!!!!')
    # exit()

    # FF.combine([sys.argv[2]])
    # system = FF.apply(system)

    write_lammps_data_file(sys.argv[1], system, box, FF=False)

