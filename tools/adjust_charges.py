#!/usr/bin/env

import numpy as np
import sys
import copy
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF
from asmbox.asm import charge_in_region, balance_walls, balance_system_charge, add_vacuum


if __name__ == '__main__':

    print('\nCan only be applied in the z direction and with two slabs.\nNeeds modification for other geometries.\n')

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    remove = sys.argv[6]

    xMin = sys.argv[7]
    xMax = sys.argv[8]
    yMin = sys.argv[9]
    yMax = sys.argv[10]
    zMin = sys.argv[11]
    zMax = sys.argv[12]

    region = np.array([[float(xMin), float(xMax)],
                     [float(yMin), float(yMax)],
                     [float(zMin), float(zMax)]])

    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)


    # make the algorithm below region dependednt
    system, atoms = balance_system_charge(system, atoms, region, remove, region, remove)



    # confirm wall charges
    charge1 = charge_in_region(system, atoms, region, remove=remove)

    print('\nConfirmation that system is indeed charge neutral')
    print(charge1)


    system = atoms.update(system)
    FF.combine(['interface', 'tip4p2005'])
    system = FF.apply(system)

    write_lammps_data_file(outfile, system, box)
