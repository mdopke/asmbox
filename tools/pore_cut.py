#!/usr/bin/env

import numpy as np
import sys
import copy
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF

from asmbox.asm import remove_atoms, configure_surface, charge_in_region


if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    remove = sys.argv[6]

    try:
        xMin = sys.argv[7]
        xMax = sys.argv[8]
        yMin = sys.argv[9]
        yMax = sys.argv[10]
        zMin = sys.argv[11]
        zMax = sys.argv[12]

        region = np.array([[float(xMin), float(xMax)],
                           [float(yMin), float(yMax)],
                           [float(zMin), float(zMax)]])

    except:
        x0 = sys.argv[7]
        y0 = sys.argv[8]
        z0 = sys.argv[9]
        r = sys.argv[10]

        region = np.array([float(x0), float(y0), float(z0), float(r)])

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    atoms.assign_indices(system)

    print('Removing Si in cube\n')
    for i in np.linspace(-25, 25, 100):
        region_ = np.array([region[0], region[1], i, region[3]])
        temp = remove_atoms(system, atoms.indices['Sib'], region_, remove=remove)
        atoms.indices['Sib'] = copy.deepcopy(temp)
    print('')

    region_ = np.array([[-region[3], region[3]],
                        [-region[3], region[3]],
                        [-100, 100]])
    system, atoms = configure_surface(system, atoms, region_, remove=remove)

    system = atoms.update(system)

    FF.combine(['interface', 'tip4p2005'])
    system = FF.apply(system)

    region_ = np.array([[-100, 100],
                        [-100, 100],
                        [-100, 100]])
    charge1 = charge_in_region(system, atoms, region_, remove=remove)

    print('\nConfirmation that system is indeed charge neutral')
    print(charge1)

    write_lammps_data_file(outfile, system, box)

