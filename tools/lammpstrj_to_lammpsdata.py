from asmbox.mdio2 import write_lammps_data_file, read_lammps_data_file
from asmbox.mdio import read_frame_lammpstrj
import numpy as np
import sys
import copy

def read_traj(fileI, vels=True):
    '''
    Function to read trajectory file frame by frame
    data is stored in xyz and type dictionaries
    all keys are stored in steps

    :param fileI:
    :return:
    '''
    xyz = {}
    types = {}
    box = {}
    vxyz = {}
    steps = []

    READING = True
    with open(fileI, 'r') as f:
        while READING:
            try:
                if vels:
                    xyz_, types_, step, box_, vxyz_ = read_frame_lammpstrj(f, read_velocities=True)
                    vxyz[step] = vxyz_ 
                else:
                    xyz_, types_, step, box_ = read_frame_lammpstrj(f, read_velocities=False)
                xyz[step] = xyz_
                types[step] = types_
                steps.append(step)
                box[step] = box_
                print('Reading time %i\r' % step, end='')
            except:
                READING = False
    print('')

    return xyz, types, steps, box, vxyz

if len(sys.argv) < 4:
    print('Requires 3 inputs:')
    print('1) lammps input file')
    print('2) lammps trajectory file')
    print('3) output filename')
    print('optional 4) frame to be written')
    exit()
vels = False

try:
    frame = int(sys.argv[4])
except:
    frame = -1

system, box = read_lammps_data_file(sys.argv[1])

xyz, types, steps, _, vxyz = read_traj(sys.argv[2], vels=vels)
if frame != -1:
    system.xyz = xyz[frame]
    if vels:
        system.vel = vxyz[frame]

else:
    system.xyz = xyz[steps[-1]]
    if vels:
        system.vel = vxyz[steps[-1]]

write_lammps_data_file(sys.argv[3], system, box)




