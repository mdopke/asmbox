#!/usr/bin/env

import numpy as np
import copy
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file



if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    buffer = sys.argv[6]
    axis = sys.argv[7]

    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    max2 = np.max(system.xyz[:, int(axis)])
    min2 = np.min(system.xyz[:, int(axis)])

    box.maxs[int(axis)] = max2 + float(buffer)
    box.mins[int(axis)] = min2 - float(buffer)

    box.lengths[int(axis)] = box.maxs[int(axis)] - box.mins[int(axis)]

    write_lammps_data_file(outfile, system, box)

