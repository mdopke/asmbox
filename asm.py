#!/usr/bin/env

import numpy as np
import copy
import math
import itertools
import asmbox.indices as Indices
import matplotlib.pyplot as plt

from asmbox.functions import *
import operator

# ==================================================================================================================== #
# Find atoms with specified coordination numbers
# ==================================================================================================================== #


def check_arguments(system, region, remove):
    '''
    Function that checks on inputs to be consistent

    :param system:
    :param region:
    :param remove:
    :return:
    '''

    # functions require bonds to exist
    if not system.bonds.shape:
        print('This function requires the bonds to be specified in system.bonds')
        print('Exiting function without return values')
        exit()

    # recognize flag
    if remove not in ['all', 'inside', 'outside']:
        print('Not recognized remove flag: %s' % remove)
        exit()

    # assign region style
    if type(region) == np.ndarray:
        if region.shape == (3, 2):
            style = 'cubic'
            if region[0, 0] > region[0, 1] or region[1, 0] > region[1, 1] or region[2, 0] > region[2, 1]:
                print('Region settings not supported. xyzlo < xyzhi required')
                exit()
        elif region.shape == (4,):
            style = 'sphere'
        else:
            style='all'
    else:
        style = 'all'

    dic = {'<': operator.lt,
           '<=': operator.le,
           '==': operator.eq,
           '=': operator.eq,
           '>': operator.gt,
           '>=': operator.ge}

    if remove == 'inside':
        fun = dic['<']
    elif remove == 'outside':
        fun = dic['>']
    else:
        fun = 'all'

    return style, fun


def find_coordinated_atoms_all(system, atoms1, atoms2, coordination):

    '''
    Function to find atoms with more or equal coordination number as specified
    Requires bonds to be defined

    :param system:
    :param atoms1:
    :param atoms2:
    :param coordination:
    :return:
    '''

    dic = {'<': operator.lt,
           '<=': operator.le,
           '==': operator.eq,
           '=': operator.eq,
           '>': operator.gt,
           '>=': operator.ge}

    try:
        fun = dic[coordination[:2]]
        CN = int(coordination[2:])
        sym = coordination[:2]
    except:
        fun = dic[coordination[:1]]
        CN = int(coordination[1:])
        sym = coordination[:1]


    # make deep copies of variables to avoid over writing
    atoms1 = copy.deepcopy(atoms1)
    atoms2 = copy.deepcopy(atoms2)


    c = 0
    atoms3 = []

    # iterate through atom indices we want to remove
    for i in copy.deepcopy(atoms1):

        bond_partners = find_bond_partners(system, i)

        bond_counter = 0

        for partner in bond_partners:
            if partner in atoms2:

                # if bonded to desired atom ids add to bond counter
                bond_counter += 1


        if len(bond_partners) == 0:
            print('Appears atom index %i had no bond at all' % i)
            print('It is recomennded to review atom configuration')
            cont = input('Continue with removing atoms (y/n)')

            while cont.lower() != 'y':

                if cont.lower() == 'n':

                    print('Exiting function without removing atoms')

                    return None

                else:
                    cont = input('Are you sure you want to continue? (y/n): ')

        if fun(bond_counter, CN):
            c += 1
            print('Coordination %s %i: counter: %i\r' % (sym, CN, c), end='')

            atoms1.remove(i)
            atoms3.append(i)

    print('')

    return atoms1, atoms3




def find_coordinated_atoms(system, atoms1, atoms2, coordination, region, remove='inside'):
    '''
    Function that removes atoms that are no longer bonded to any other atom


    :param system:
    :param atoms1:
    :param atoms2:
    :param coordination: 0 removed nonbonded 1 with coordination fewer than 1
    :param region:
    :param type:
    :param remove:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    # make deepcopies to avoid over writing original variables
    region = copy.deepcopy(region)
    atoms1 = copy.deepcopy(atoms1)
    atoms2 = copy.deepcopy(atoms2)
    system = copy.deepcopy(system)

    if remove == 'all' or style =='all':

        atoms1, atoms4 = find_coordinated_atoms_all(system, atoms1, atoms2, coordination)

        return atoms1, atoms4
    else:
        # find atom indices of atoms1 within specified region
        atoms3 = []

        for i in copy.deepcopy(atoms1):

            if style == 'cubic':

                if fun(region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], region[0, 1]) and \
                        fun(region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], region[1, 1]) and \
                        fun(region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], region[2, 1]):

                        atoms3.append(i)
                        atoms1.remove(i)

            elif style == 'sphere':

                    if fun((system.xyz[i, 0] - region[0]) ** 2 + \
                            (system.xyz[i, 1] - region[1]) ** 2 + \
                            (system.xyz[i, 2] - region[2]) ** 2, region[3] ** 2):

                            atoms3.append(i)
                            atoms1.remove(i)

    atoms3, atoms4 = find_coordinated_atoms_all(system, atoms3, atoms2, coordination)

    return atoms1 + atoms3, atoms4


# ==================================================================================================================== #
# Removal routines
# ==================================================================================================================== #


def remove_atoms(system, atoms, region, remove='inside'):

    '''
    Function to removes atoms from a list (atoms)
    within (inside or outside)
    a specified region (cubic or spherical)

    :param system:
    :param box:
    :param atoms: list of indices
    :param region:
    :param type:
    :param remove:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    region = copy.deepcopy(region)
    atoms = copy.deepcopy(atoms)
    # system = copy.deepcopy(system)

    if style == 'cubic':

        c = 0

        for i in copy.deepcopy(atoms):

            if fun(region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], region[0, 1]) and \
                    fun(region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], region[1, 1]) and \
                    fun(region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], region[2, 1]):

                c += 1
                print('Removing atoms %i\r' % c, end='')

                atoms.remove(i)

    elif style == 'sphere':

        c = 0

        for i in copy.deepcopy(atoms):

            if fun((system.xyz[i, 0] - region[0]) ** 2 + \
                    (system.xyz[i, 1] - region[1]) ** 2 + \
                    (system.xyz[i, 2] - region[2]) ** 2, region[3] ** 2):

                c += 1
                print('Removing atoms %i\r' % c, end='')

                atoms.remove(i)

    print('')

    return atoms

def removeSiddd(system, atoms, region, remove='inside'):

    '''
    Function that removes all SiO3 within a specified region. Configured to only work for danglings

    :param system:
    :param atoms:
    :param cube:
    :param type:
    :param remove:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    dic = {'inside': operator.add,
           'outside': operator.sub}

    system = copy.deepcopy(system)
    atoms = copy.deepcopy(atoms)
    region = copy.deepcopy(region)

    lenall0 = len(atoms.all())
    lenall = 0
    c = 0

    # need to do this below procedure until convergence
    while lenall - lenall0 != 0:

        lenall0 = len(atoms.all())

        # remove SidOd3 # make a function find over coordinates
        print('Removing SidOd3')

        if style=='all' or remove=='all':
            _, temp = find_coordinated_atoms(system, atoms.select('Sid'), atoms.indices['Od'], '>=3',
                                             0, remove=remove)

        elif style=='cubic':
            _, temp = find_coordinated_atoms(system, atoms.select('Sid'), atoms.indices['Od'], '>=3',
                                             dic[remove](region, np.array([-1, 1]) * c), remove=remove)

        elif style == 'sphere':
            _, temp = find_coordinated_atoms(system, atoms.select('Sid'), atoms.indices['Od'], '>=3',
                                             dic[remove](region, np.array([0, 0, 0, 1]) * c), remove=remove)

        for t in temp:
            # remove silica
            atoms.indices['Sidd'].remove(t)

            # find all attached oxygens
            bond_partners = find_bond_partners(system, t)

            for partner in bond_partners:

                # if oxygen was dangling remove
                if partner in atoms.indices['Od']:
                    atoms.indices['Od'].remove(partner)

                # elif in bulk check what it becomes
                elif partner in atoms.indices['Ob']:

                    # check partners
                    bond_partners_ = find_bond_partners(system, partner)

                    if sum(partner_ in atoms.select('Si') for partner_ in bond_partners_) <= 1:
                        atoms.indices['Ob'].remove(partner)
                        atoms.indices['Od'].append(partner)

                        for partner_ in bond_partners_:
                            if partner_ in atoms.indices['Sib']:
                                atoms.indices['Sib'].remove(partner_)
                                atoms.indices['Sid'].append(partner_)
                            elif partner_ in atoms.indices['Sid']:
                                atoms.indices['Sid'].remove(partner_)
                                atoms.indices['Sidd'].append(partner_)
                            elif partner_ in atoms.indices['Sidd']:
                                # do nothing as it will be balanced in the next iteration
                                continue

        lenall = len(atoms.all())
        c += 3

        # print(lenall0, lenall)
    return system, atoms


def configure_surface(system, atoms, region, remove='inside'):
    '''
    Function to remove atoms within a cube

    :param system:
    :param atoms:
    :param cube:
    :param type:
    :param remove:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    dic = {'inside': operator.add,
           'outside': operator.sub}


    system = copy.deepcopy(system)
    atoms = copy.deepcopy(atoms)
    region = copy.deepcopy(region)

    # remove uncoordinated O
    print('Removing uncoordinated O')
    if style == 'cubic':
        atoms.indices['Ob'], _ = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.indices['Sib'], '<=0',
                                                        dic[remove](region, np.array([-1, 1]) * 3), remove=remove)

    elif style == 'sphere':
        atoms.indices['Ob'], _ = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.indices['Sib'], '<=0',
                                                        dic[remove](region, np.array([0, 0, 0, 1]) * 3), remove=remove)

    # Identify Od
    print('Identifying Od')
    if style == 'cubic':
        atoms.indices['Ob'], atoms.indices['Od'] = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.indices['Sib'], '<=1',
                                                                          dic[remove](region, np.array([-1, 1]) * 6), remove=remove)

    elif style == 'sphere':
        atoms.indices['Ob'], atoms.indices['Od'] = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.indices['Sib'], '<=1',
                                                                          dic[remove](region, np.array([0, 0, 0, 1]) * 6), remove=remove)


    # identify Sidd
    print('Identifying Sidd')
    if style == 'cubic':
        atoms.indices['Sib'], atoms.indices['Sidd'] = find_coordinated_atoms(system, atoms.indices['Sib'], atoms.indices['Od'], '>=2',
                                                                             dic[remove](region, np.array([-1, 1]) * 9), remove=remove)

    elif style == 'sphere':
        atoms.indices['Sib'], atoms.indices['Sidd'] = find_coordinated_atoms(system, atoms.indices['Sib'], atoms.indices['Od'], '>=2',
                                                                             dic[remove](region, np.array([0, 0, 0, 1]) * 9), remove=remove)

    # identify Sid
    print('Identifying Sid')
    if style == 'cubic':
        atoms.indices['Sib'], atoms.indices['Sid'] = find_coordinated_atoms(system, atoms.indices['Sib'], atoms.indices['Od'], '>=1',
                                                                            dic[remove](region, np.array([-1, 1]) * 9), remove=remove)

    elif style == 'sphere':
        atoms.indices['Sib'], atoms.indices['Sid'] = find_coordinated_atoms(system, atoms.indices['Sib'], atoms.indices['Od'], '>=1',
                                                                            dic[remove](region, np.array([0, 0, 0, 1]) * 9), remove=remove)


    if style == 'cubic':
        system, atoms = removeSiddd(system, atoms, dic[remove](region, np.array([-1, 1]) * 9), remove=remove)

    elif style == 'sphere':
        system, atoms = removeSiddd(system, atoms, dic[remove](region, np.array([0, 0, 0, 1]) * 9), remove=remove)


    return system, atoms

# ==================================================================================================================== #
# Protonation routines
# ==================================================================================================================== #


def cylinder_n(p0, p1, r):
    '''
    Function to determine circle for protonation

    :param p0: DO location
    :param p1: H (centre of circle) location
    :param r: radius as a function of DOH angle
    :return:
    '''

    import numpy as np

    # vector continuation from Si-O
    v = p1 - p0
    # find magnitude of vector
    mag = np.linalg.norm(v)
    # unit vector in direction of axis
    v /= mag
    # make some vector not in the same direction as v
    not_v = np.array([1, 0, 0])
    if (v.__abs__() == not_v.__abs__()).all():
        not_v = np.array([0, 1, 0])

    # make vector perpendicular to v
    n1 = np.cross(v, not_v)
    # normalize n1
    n1 /= np.linalg.norm(n1)

    # make unit vector perpendicular to v and n1
    n2 = np.cross(v, n1)

    # surface ranges over 0 to 2*pi
    theta = np.linspace(0, 2 * np.pi, 100)

    # generate coordinates for circle edges

    x, y, z = [p1[i] + r * np.sin(theta) * n1[i] + r * np.cos(theta) * n2[i] for i in [0, 1, 2]]

    p = np.zeros((len(x), 3))
    for i in range(len(x)):
        p[i] = [x[i], y[i], z[i]]

    return p


def control_distances(atoms, system, box, p2):
    '''
    Annoying function used to check distances from new hydrogen to
    second nearest oxygen
    nearest silicon
    nearest hydrogen

    :param atoms:
    :param system:
    :param box:
    :param p2:
    :return:
    '''

    import numpy as np

    dds = []
    for idx in atoms.select('O'):#atoms.indices['Ob'] + atoms.indices['Od'] + atoms.indices['Os']:
        dds.append(distance(system.xyz[idx], p2, box))
    dds = np.asarray(dds)
    dd_1 = dds[np.argsort(dds)[1]]

    dds = []
    for idx in atoms.select('Si'):#atoms.indices['Sib']+atoms.indices['Sid']+atoms.indices['Sis']:
        dds.append(distance(system.xyz[idx], p2, box))
    dds = np.asarray(dds)
    dd_2 = dds[np.argsort(dds)[0]]

    try:
        dds = []
        for idx in atoms.indices['Hs']:
            dds.append(distance(system.xyz[idx], p2, box))
        dds = np.asarray(dds)

        dd_3 = dds[np.argsort(dds)[0]]
    except:
        dd_3 = 3.0

    return dd_1, dd_2, dd_3


def protonate(system, box, atoms, bond_type):
    '''
    Function to protonate dangling oxygens.
    Requires function cylinder_n

    :param data: ase atomic data file
    :param bonds: bonds dictionary (rewrite to system.bonds array style)
    :param Si: indices list of silicon
    :param DSi: indices list of dangling silicon
    :param O: indices list of oxygen
    :param DO: indices list of dangling oxygen
    :return:
    '''

    atoms = copy.deepcopy(atoms)
    system = copy.deepcopy(system)
    box = copy.deepcopy(box)

    # print('Note that after this function all DO are silanol oxygens but keep the label DO')

    import numpy as np

    # protonate oxygens to achieve real neutrality
    c = 0
    for i in atoms.indices['Od']:

        c += 1
        print("Protonating: %d / %d\r" % (c, len(atoms.indices['Od'])), end="")

        bond_partners = find_bond_partners(system, i)

        if any(partners in atoms.indices['Sib'] for partners in bond_partners):
            print('Bulk silicon in list as bonded to dangling oxygen')
            print('Expand region')
            return None, None, None, None, None
        if len(bond_partners) > 1:
            print('More than one dangling silicon connected to supposed dangling oxygen')
            return None, None, None, None, None
        if len(bond_partners) < 1:
            print('No bond found')
            return None, None, None, None, None

        # # find bond partner of dangling oxygen
        # bond_i = []
        #
        # for bcol in [[1, 2], [2, 1]]:
        #
        #     k = np.where(system.bonds[:, bcol[0]] == i + 1)[0]  # plus one due to indexing issue
        #
        #     if len(k) > 0:
        #
        #         for k_ in k:
        #
        #             bond_append = system.bonds[k_, bcol[1]]
        #
        #             if bond_append - 1 in atoms.indices['Sid']:
        #                 bond_i.append(bond_append)
        #
        #             elif bond_append - 1 in atoms.indices['Sib']:
        #                 print('Bulk silicon in list as bonded to dangling oxygen')
        #                 print('Expand region')
        #                 return None, None, None, None, None



        DSii = bond_partners[0]

        v = vector(system.xyz[DSii, :], system.xyz[i, :], box)  # should be vector
        v /= np.linalg.norm(v)

        p0 = system.xyz[i]
        p1 = p0 + v * np.cos(np.pi - 115 / 180 * np.pi)
        radius = np.linalg.norm(v * np.sin(np.pi - 115 / 180 * np.pi))

        p = cylinder_n(p0, p1, radius)

        iter = 0
        protonated = False
        while not protonated:
            p2 = p[np.random.randint(0, len(p), 1)[0]]
            p2 = wrap_point(p2, box)
            # p2 = np.where(np.abs(p2) > 0.5 * box.lengths, box.lengths - np.abs(p2), p2)

            dd_1, dd_2, dd_3 = control_distances(atoms, system, box, p2)

            if dd_1 > 1.0 and dd_2 > 0.8 and dd_3 > 1.0:
                protonated = True

            iter += 1
            if iter == 100:
                print('ERROR. Distances HO: %f HSi %f HH %f' % (dd_1, dd_2, dd_3))
                exit()

        num = len(system.types)

        system = insert_atom(system, p2, 9, 0.4, 1, bond_type, [i])

        # system.types = np.concatenate((system.types, np.array([9])))
        # system.charges = np.concatenate((system.charges, np.array([0])))
        # system.xyz = np.vstack((system.xyz, p2))
        #
        # system.resids = np.concatenate((system.resids, np.array([1])))
        # system.bonds = np.vstack((system.bonds, np.array([bond_type, i + 1, num+1])))
        # system.bonds = np.vstack((system.bonds, np.array([bond_type, i + 1, num+1])))

        # add resids and bonds

        atoms.indices['Hs'].append(num)

    print('')

    atoms.indices['Os'] = atoms.indices['Od']
    atoms.indices['Od'] = []

    atoms.indices['Sis'] = atoms.indices['Sid']
    atoms.indices['Sid'] = []

    atoms.indices['Siss'] = atoms.indices['Sidd']
    atoms.indices['Sidd'] = []

    return system, atoms




def lhs_deprotonate(system, box, idx, num_deprotonate, criterion):

    from pyDOE import lhs

    def duplicates(array):

        uniques, indices = np.unique(array, return_index=True)

        j = []
        for i in range(len(array)):
            if i not in indices:
                j.append(i)

        return array[j], j

    def distances_(p, pts, box):

        d = np.abs(pts - p)

        for j in range(len(d)):
            for k in range(2):
                d[j, k] = np.where(d[j, k] > 0.5 * box.lengths[k] / 2, box.lengths[k] - d[j, k], d[j, k])

        return np.sqrt((d ** 2).sum(axis=-1))


    def repeated(records_array):
        idx_sort = np.argsort(records_array)
        sorted_records_array = records_array[idx_sort]
        vals, idx_start, count = np.unique(sorted_records_array, return_counts=True, return_index=True)

        # sets of indices
        res = np.split(idx_sort, idx_start[1:])
        # filter them with respect to their size, keeping only items occurring more than once

        vals = vals[count > 1]
        res = filter(lambda x: x.size > 1, res)

        return vals, list(res)[0]

    length, axis = duplicates(box.lengths)

    # uniques, indices = np.unique(box.lengths, return_index=True)
    val, indices = repeated(box.lengths)

    # print(val, indices)

    xyz = (lhs(2, samples=num_deprotonate, criterion=criterion) - 0.5) * length


    temp = []

    for i in range(num_deprotonate):
        # print(i)

        d = distances_(xyz[i, :2], system.xyz[idx, :2][:, indices], box)

        # find the nearest dangling site
        idx_ = np.argsort(d)

        # make sure there is no other dangling site nearby
        d_ = 0
        j = 0
        if len(temp) > 0:
            dist_ = 9
            while d_ < dist_:

                idxes = idx[idx_[j]]

                if idxes not in temp:
                    d_ = min(distances_(system.xyz[idxes, :][indices], system.xyz[temp, :][:, indices], box))
                    if j == len(idx_)-1:
                        j = 0
                        dist_ -= 0.1
                j += 1


        temp.append(idx[idx_[j-1]])

        # the line below removed it from the checker thus destroying my indices
        # idx = np.delete(idx, idx_[j])

    return np.asarray(temp)


def deprotonate(system, box, atoms, num_deprotonate, region, remove='inside'):
    '''
    Function to deprotonate in a specified region num_deprotonate atoms

    :param system:
    :param box:
    :param atoms:
    :param num_deprotonate:
    :param region:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    indices = []
    for i in atoms.indices['Hs']:

        if style == 'cubic':

            if fun(region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], region[0, 1]) and \
                    fun(region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], region[1, 1]) and \
                    fun(region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], region[2, 1]):

                indices.append(i)

        elif style == 'sphere':
            if fun((system.xyz[i, 0] - region[0]) ** 2 + \
                    (system.xyz[i, 1] - region[1]) ** 2 + \
                    (system.xyz[i, 2] - region[2]) ** 2, region[3] ** 2):

                indices.append(i)

    if len(indices) < num_deprotonate:
        print('Cannot find enough silanols to deprotonate within specified region.')
        print('Found %i silanol groups' % len(indices))
        print('EXITING PYTHON')
        exit()

    repeat = 'y'
    while repeat == 'y':
        plt.figure()

        deprotonate = lhs_deprotonate(system, box, indices, num_deprotonate, 'center')

        plt.scatter(system.xyz[deprotonate, 0], system.xyz[deprotonate, 1])

        plt.scatter(system.xyz[deprotonate, 0] - box.lengths[0], system.xyz[deprotonate, 1] - box.lengths[1])
        plt.scatter(system.xyz[deprotonate, 0] - box.lengths[0], system.xyz[deprotonate, 1])
        plt.scatter(system.xyz[deprotonate, 0] - box.lengths[0], system.xyz[deprotonate, 1] + box.lengths[1])

        plt.scatter(system.xyz[deprotonate, 0], system.xyz[deprotonate, 1] - box.lengths[1])
        plt.scatter(system.xyz[deprotonate, 0], system.xyz[deprotonate, 1] + box.lengths[1])

        plt.scatter(system.xyz[deprotonate, 0] + box.lengths[0], system.xyz[deprotonate, 1] - box.lengths[1])
        plt.scatter(system.xyz[deprotonate, 0] + box.lengths[0], system.xyz[deprotonate, 1])
        plt.scatter(system.xyz[deprotonate, 0] + box.lengths[0], system.xyz[deprotonate, 1] + box.lengths[1])

        plt.plot([box.mins[0], box.mins[0]], [box.mins[1], box.maxs[1]], 'k--')
        plt.plot([box.maxs[0], box.maxs[0]], [box.mins[1], box.maxs[1]], 'k--')

        plt.plot([box.mins[0], box.maxs[0]], [box.mins[1], box.mins[1]], 'k--')
        plt.plot([box.mins[0], box.maxs[0]], [box.maxs[1], box.maxs[1]], 'k--')

        plt.xlim(box.mins[0]-box.lengths[0], box.maxs[0]+box.lengths[0])
        plt.ylim(box.mins[1]-box.lengths[1], box.maxs[1]+box.lengths[1])
        print('See figure for distribution')
        print('Close to continue')
        print('ctrl+c to break')

        plt.grid(True)
        plt.show()

        repeat = input('Repeat deprotonation y/n: ')

        while repeat != 'y' and repeat != 'n':
            repeat = input('Repeat deprotonation y/n: ')

    for j in deprotonate:
        atoms.indices['Hs'].remove(j)

        bond_partner = find_bond_partners(system, j)[0]

        atoms.indices['Os'].remove(bond_partner)
        atoms.indices['Od'].append(bond_partner)

        bond_partner = find_bond_partners(system, bond_partner)[0]

        if bond_partner in atoms.indices['Siss']:
            atoms.indices['Siss'].remove(bond_partner)
            atoms.indices['Sid'].append(bond_partner)
        elif bond_partner in atoms.indices['Sis']:
            atoms.indices['Sis'].remove(bond_partner)
            atoms.indices['Sid'].append(bond_partner)
        elif bond_partner in atoms.indices['Sid']:
            atoms.indices['Sid'].remove(bond_partner)
            atoms.indices['Sidd'].append(bond_partner)


    return atoms



# ==================================================================================================================== #
# bridging routine
# ==================================================================================================================== #


def adjust_silanol_density(system, box, atoms, area, silanol_density, bond_type, region, remove='inside', get_bridged=True):


    style, fun = check_arguments(system, region, remove)

    system = copy.deepcopy(system)

    isolated, vicinals, geminals = silanol_classification(system, atoms)

    if style == 'all' or remove == 'all':

        atoms_inside = copy.deepcopy(atoms)


    else:
        atoms_inside = Indices.Indices()

        for _, atr in enumerate(atoms.atom_types):

            for i in copy.deepcopy(atoms.indices[atr]):

                if style =='cubic':

                    if fun(region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], region[0, 1]) and \
                            fun(region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], region[1, 1]) and \
                            fun(region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], region[2, 1]):

                        atoms_inside.indices[atr].append(i)
                        atoms.indices[atr].remove(i)

                elif style == 'sphere':

                    if fun((system.xyz[i, 0] - region[0]) ** 2 + \
                           (system.xyz[i, 1] - region[1]) ** 2 + \
                           (system.xyz[i, 2] - region[2]) ** 2, region[3] ** 2):

                        atoms_inside.indices[atr].append(i)
                        atoms.indices[atr].remove(i)

    # Above is routine for selection of region

    dd_SiSi = 4.0
    BO = []
    BSi = []

    area *= 0.01

    print('\nCurrent silanol density %f' % (len(atoms_inside.indices['Od']) / area))

    # define silanol density difference to mark number of bridges
    silanol_density_diff = len(atoms_inside.indices['Od']) - silanol_density * area
    num_bridges = int(round(silanol_density_diff / 4) * 2)

    # Start bridging
    if num_bridges != 0:
        print('\nAdjusting Silanol density ')

        if num_bridges > 0:

            # iterate through all bridges
            for bridges in range(num_bridges):

                # define iteration parameters
                iter = 0
                maxIter = len(atoms_inside.indices['Od']) - len(BO)
                selection = geminals

                bridged = False
                while not bridged:
                    print("Bridged: %3d / %3d, Iteration: %4d / %4d, Maximum bridging length %1.1f\r" % (bridges + 1, num_bridges, iter, maxIter, dd_SiSi), end="")

                    # increase distance between Si atoms and reset iterator
                    iter += 1
                    if iter > maxIter:
                        dd_SiSi += 0.1
                        iter = 0

                    repeat = True
                    while repeat:
                        # select a random dangling oxygen
                        j = np.random.choice(atoms_inside.indices['Od'])

                        # make sure dangling oxygen is a geminal
                        possibilities = maxIter
                        while j not in selection:
                            j = np.random.choice(atoms_inside.indices['Od'])
                            possibilities -= 1

                            # if no geminals left allow all combinations
                            if possibilities <= 0:
                                selection = geminals + isolated + vicinals


                        # find bond partners
                        bond_partners = find_bond_partners(system, j)

                        bond_j = []
                        rep_counter = 0
                        # check bond partners
                        for partner in bond_partners:

                            if partner in atoms_inside.select('Sid'):  # atoms_inside.indices['Sid']:
                                bond_j.append(partner)

                            elif partner in BSi:
                                rep_counter += 1

                            elif partner in atoms_inside.indices['Sib']:
                                print('Bulk silicon in list as bonded to dangling oxygen')

                            elif partner in atoms.select('Sid'):  # atoms.indices['Sid']:
                                print('Using dangling silicon outside of specified region')
                                print('Expand region')
                                return None, None, None, None, None
                                # bond_j.append(bond_append)

                            elif partner in atoms.indices['Sib']:
                                print('Bulk silicon outside of region in list as bonded to dangling oxygen')

                        if rep_counter == 0:
                            repeat = False



                    # should only have one bond partner
                    if len(bond_j) > 1:
                        print('Not dangling oxygen in list')

                    # assign bond partner
                    Sij = bond_j[0]

                    # iterate through all dangling oxygens
                    for i in randomly(copy.deepcopy(atoms_inside.indices['Od'])):

                        # dangling oxygens have to be different
                        selection = geminals
                        if i != j: # and (i in selection or j in selection):

                            proceed = True

                            # find bond partners
                            bond_partners = find_bond_partners(system, i)

                            bond_i = []

                            # check bond partners
                            for partner in bond_partners:

                                if partner in atoms_inside.select('Sid'):  # atoms_inside.indices['Sid']:
                                    bond_i.append(partner)

                                elif partner in BSi:
                                    proceed = False

                                elif partner in atoms_inside.indices['Sib']:
                                    print('Bulk silicon in list as bonded to dangling oxygen')

                                elif partner in atoms.select('Sid'):  # atoms.indices['Sid']:
                                    print('Using dangling silicon outside of specified region')
                                    print('Expand region')
                                    return None, None, None, None, None
                                    # bond_i.append(bond_append)

                                elif partner in atoms.indices['Sib']:
                                    print('Bulk silicon outside of region in list as bonded to dangling oxygen')

                            if proceed:
                                # should only have 1 bond partner
                                if len(bond_i) > 1:
                                    # print(bond_j, bond_i)
                                    print('Not dangling oxygen in list')

                                # assign bond partner
                                Sii = bond_i[0]


                                # if the two dangling oxygens do not share a silicon do bridging
                                if Sii != Sij:

                                    # check whether those Si atoms share a bond

                                    # find bond partners of Sii (can be up to 4)
                                    bond_ii = []

                                    for bcol in [[1, 2], [2, 1]]:

                                        k = np.where(system.bonds[:, bcol[0]] == Sii + 1)[0]  # plus one due to indexing issue

                                        if len(k) > 0:

                                            for k_ in k:

                                                bond_append = system.bonds[k_, bcol[1]]

                                                if bond_append - 1 in atoms_inside.indices['Ob'] + BO:
                                                    bond_ii.append(bond_append)

                                    # find bond partners of Sij
                                    bond_ij = []

                                    for bcol in [[1, 2], [2, 1]]:

                                        k = np.where(system.bonds[:, bcol[0]] == Sij + 1)[0]  # plus one due to indexing issue

                                        if len(k) > 0:

                                            for k_ in k:

                                                bond_append = system.bonds[k_, bcol[1]]

                                                if bond_append - 1 in atoms_inside.indices['Ob'] + BO:
                                                    bond_ij.append(bond_append)

                                    # make sure they do not share a bond
                                    if not any(bond in bond_ii for bond in bond_ij):

                                        d_SiiSij = distance(system.xyz[Sii], system.xyz[Sij], box)

                                        if d_SiiSij < dd_SiSi:

                                            # determine vector between Sii and Sij to place new oxygen
                                            v = vector(system.xyz[Sii], system.xyz[Sij], box)

                                            # find in between point and adjust in case of pbc
                                            p = system.xyz[Sii] + v / 2
                                            p = wrap_point(p, box)

                                            # add a new atom with a new index
                                            new_atom_id = len(system.types)

                                            BO.append(new_atom_id)

                                            # mark Sii and Sij as bonded silicon
                                            BSi.append(Sii)
                                            BSi.append(Sij)

                                            # Do not use Sii and Sij again for bridging
                                            if Sii in atoms_inside.indices['Sid']:
                                                atoms_inside.indices['Sid'].remove(Sii)
                                            elif Sii in atoms_inside.indices['Sidd']:
                                                atoms_inside.indices['Sidd'].remove(Sii)

                                            if Sij in atoms_inside.indices['Sid']:
                                                atoms_inside.indices['Sid'].remove(Sij)
                                            elif Sij in atoms_inside.indices['Sidd']:
                                                atoms_inside.indices['Sidd'].remove(Sij)

                                            # remove the two dangling oxygens from list
                                            atoms_inside.indices['Od'].remove(i)
                                            atoms_inside.indices['Od'].remove(j)

                                            system = insert_atom(system, p, 3, -1.2, 1, bond_type, [Sii, Sij])

                                            bridged = True
                                            break

        elif num_bridges < 0:
            print('Need to implement breaking bridges')
            exit()

        print('')

    # Add silicas back to the original lists
    # distinguish silicas can be dangling or bulk
    for i in BSi:

        bond_partners = find_bond_partners(system, i)

        if sum(partner in atoms_inside.indices['Od'] for partner in bond_partners) >= 2:
            atoms_inside.indices['Sidd'].append(i)

        elif sum(partner in atoms_inside.indices['Od'] for partner in bond_partners) >= 1:
            atoms_inside.indices['Sid'].append(i)

        else:
            atoms_inside.indices['Sib'].append(i)

    for i in BO:
        atoms_inside.indices['Ob'].append(i)

    print('\nCurrent silanol density %f' % (len(atoms_inside.indices['Od']) / area))

    if style == 'all' or remove == 'all':
        atoms = atoms_inside
    else:
        for _, atr in enumerate(atoms.atom_types):
            for i in atoms_inside.indices[atr]:
                atoms.indices[atr].append(i)


    if get_bridged:
        return system, atoms, BO, BSi
    else:
        return system, atoms


# ==================================================================================================================== #
# Charge balancing operations
# ==================================================================================================================== #


def charge_in_region(system, atoms, region, remove='all'):
    '''
    Function that determines the charge in a specified region

    :param system:
    :param atoms:
    :param region:
    :param type:
    :param remove:
    :return:
    '''

    style, fun = check_arguments(system, region, remove)

    charge = 0

    if style == 'all' or remove == 'all':

        for i in copy.deepcopy(atoms.all()):
            if i in atoms.indices['Ob']:
                charge -= 1.2
            elif i in atoms.select('Si'):#atoms.indices['Sib'] + atoms.indices['Sid'] + atoms.indices['Sis']:
                charge += 2.4
            elif i in atoms.select('Od') + atoms.select('Os'):
                charge -= 0.6

    else:

        for i in copy.deepcopy(atoms.all()):
            if style == 'cubic':

                if fun(region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], region[0, 1]) and \
                        fun(region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], region[1, 1]) and \
                        fun(region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], region[2, 1]):

                    if i in atoms.indices['Ob']:
                        charge -= 1.2
                    elif i in atoms.select('Si'):  # atoms.indices['Sib'] + atoms.indices['Sid'] + atoms.indices['Sis']:
                        charge += 2.4
                    elif i in atoms.indices['Od']:
                        charge -= 0.6

            elif style == 'sphere':

                    if fun((system.xyz[i, 0] - region[0]) ** 2 + \
                            (system.xyz[i, 1] - region[1]) ** 2 + \
                            (system.xyz[i, 2] - region[2]) ** 2, region[3] ** 2):

                        if i in atoms.indices['Ob']:
                            charge -= 1.2
                        elif i in atoms.select(
                                'Si'):  # atoms.indices['Sib'] + atoms.indices['Sid'] + atoms.indices['Sis']:
                            charge += 2.4
                        elif i in atoms.indices['Od']:
                            charge -= 0.6


    return round(charge, 2)


def balance_walls(system, atoms, axis):
    '''
    This function balances the charges between two (almost) symmetrical walls
    separated a distance
    and with vacuum throguh the boundary

    :param system:
    :param atoms:
    :param axis:
    :return:
    '''

    system = copy.deepcopy(system)
    atoms = copy.deepcopy(atoms)

    _, affectedO = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.select('Si'), '<=1',
                                          0, remove='inside')

    affectedO1 = []
    affectedO2 = []

    for aff in affectedO:
        if system.xyz[aff, axis] < 0:
            affectedO1.append(aff)
        elif system.xyz[aff, axis] > 0:
            affectedO2.append(aff)

    cube = np.zeros((3, 2))
    cube[:, 0] = -1000
    cube[:, 1] = 1000
    cube[axis, 0] = -1000
    cube[axis, 1] = 0

    # find charge per region
    charge1 = charge_in_region(system, atoms, cube, remove='inside')
    charge2 = charge_in_region(system, atoms, cube[:, ::-1] * -1, remove='inside')



    while (np.abs(charge1) > np.abs(charge1 + charge2) or np.abs(charge2) > np.abs(charge1 + charge2)) and np.abs(charge1) != 0.6 and np.abs(charge2) != 0.6:

        if charge1 < charge2:

            j = np.random.choice(affectedO1)
            affectedO1.remove(j)
            system.xyz[j, axis] *= -1#+= box.lengths[axis]

        elif charge1 > charge2:
            j = np.random.choice(affectedO2)
            affectedO2.remove(j)
            system.xyz[j, axis] *= -1#-= box.lengths[axis]


        charge1 = charge_in_region(system, atoms, cube, remove='inside')
        charge2 = charge_in_region(system, atoms, cube[:, ::-1] * -1, remove='inside')

        print(charge1, charge2)

    return system, atoms




def balance_system_charge(system, atoms, inspect_region, inspect_region_remove, operations_region, operations_region_remove):
    '''
        Function that attempts to balance the entire system charge

    2 regions are to be specified,
    1 to measure charge
    1 to remove atoms
    regions can be the same

    :param system:
    :param atoms:
    region to check for charge
    :param inspect_region:
    :param inspect_region_remove:
    Region in which to remove
    :param region2:
    :param remove2:
    :return:
    '''

    style, fun = check_arguments(system, operations_region, operations_region_remove)

    system = copy.deepcopy(system)
    atoms = copy.deepcopy(atoms)

    # ==================================================================================================================== #
    # Determina la carge del systema
    # ==================================================================================================================== #

    # total_charge = round(len(atoms_inside.select('Si')) * 2.4 - 1.2 * len(atoms_inside.select('O')) + 0.6 * len(atoms_inside.select('Od')), 2)

    total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)


    print('Initial system charge %f' % total_charge)

    # si la carga es diferente a 0 cambia la carga
    while total_charge != 0:

        # si es divisible por 0.6 pero no por 1.2 saca silanol
        while 10 * total_charge % 6 == 0 and not 10 * total_charge % 12 == 0:

            print('Removing silanol')

            # need to remove silanol group (OH)

            bond_partners = []
            while len(bond_partners) < 4:
                # encuentra silanol a random (elimina un silanol de un grupo Sidd, con 2 silanoles)

                atoms3 = []
                if style == 'all' or operations_region_remove =='all':
                    atoms3 = atoms.indices['Sidd']
                else:
                    for i in atoms.indices['Sidd']:
                        if style == 'cubic':

                            if fun(operations_region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], operations_region[0, 1]) and \
                                    fun(operations_region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], operations_region[1, 1]) and \
                                    fun(operations_region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], operations_region[2, 1]):

                                atoms3.append(i)

                        elif style == 'sphere':

                            if fun((system.xyz[i, 0] - operations_region[0]) ** 2 + \
                                   (system.xyz[i, 1] - operations_region[1]) ** 2 + \
                                   (system.xyz[i, 2] - operations_region[2]) ** 2, operations_region[3] ** 2):

                                atoms3.append(i)

                j = np.random.choice(atoms3)
                # it matters whether this is left or right for symmetric systems

                # find bond partners
                bond_partners = find_bond_partners(system, j)

            # encuentra los silanoles del Sidd
            removeOd = []
            for partner in bond_partners:

                # if partner is dangling oxygen
                if partner in atoms.indices['Od']:
                    removeOd.append(partner)

            # escoge un silanol a random
            i = np.random.choice(removeOd)

            # elimina el silanol, indice y bond
            atoms.remove(i)

            # el sidd se convierte en sid
            atoms.indices['Sidd'].remove(j)
            atoms.indices['Sid'].append(j)

            # cambia la carga
            total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)

            print(total_charge)

        # las operaciones aqui de abajo requiren vacuo
        # si la carga es positive y divisible por 24 saca un silca
        while total_charge > 0 and 10 * total_charge % 24 == 0:
            # find Sib to remove
            _, affected = find_coordinated_atoms(system, atoms.indices['Sib'], atoms.select('O'), '<=2',
                                                 operations_region, remove=operations_region_remove)

            # if founs suitable Sib remove it
            if len(affected) > 0:

                print('Removing silicon')

                repeat = True
                while repeat:
                    if len(affected) > 0:
                        j = np.random.choice(affected)
                    else:
                        print('Found no atom to remove')
                        exit()
                    # determine partners
                    bond_partners = find_bond_partners(system, j)

                    # if any partner would be isolated at the end find a new one
                    if any(count_bonds(system, partner) < 2 for partner in bond_partners):
                        repeat = True
                    else:
                        repeat = False

                atoms.indices['Sib'].remove(j)
                # atoms_inside.indices['Ca'].append(j) # used to test where atom was

            # if not modify silanol to bulk
            else:
                print('Converting silanol oxygen to bulk oxygen')

                # need to convert silanol group (OH) to bulk

                bond_partners = []
                while len(bond_partners) < 4:
                    # encuentra silanol a random (elimina un silanol de un grupo Sidd, con 2 silanoles)

                    atoms3 = []
                    if style == 'all' or operations_region_remove == 'all':
                        atoms3 = atoms.indices['Sidd']
                    else:
                        for i in atoms.indices['Sidd']:
                            if style == 'cubic':

                                if fun(operations_region[0, 0], system.xyz[i, 0]) and fun(system.xyz[i, 0], operations_region[0, 1]) and \
                                        fun(operations_region[1, 0], system.xyz[i, 1]) and fun(system.xyz[i, 1], operations_region[1, 1]) and \
                                        fun(operations_region[2, 0], system.xyz[i, 2]) and fun(system.xyz[i, 2], operations_region[2, 1]):
                                    atoms3.append(i)

                            elif style == 'sphere':

                                if fun((system.xyz[i, 0] - operations_region[0]) ** 2 + \
                                       (system.xyz[i, 1] - operations_region[1]) ** 2 + \
                                       (system.xyz[i, 2] - operations_region[2]) ** 2, operations_region[3] ** 2):
                                    atoms3.append(i)

                    j = np.random.choice(atoms3)

                    # find bond partners
                    bond_partners = find_bond_partners(system, j)

                # encuentra los silanoles del Sidd
                removeOd = []
                for partner in bond_partners:

                    # if partner is dangling oxygen
                    if partner in atoms.indices['Od']:
                        removeOd.append(partner)

                # escoge un silanol a random
                i = np.random.choice(removeOd)

                # elimina el silanol, indice y bond
                atoms.indices['Od'].remove(i)
                atoms.indices['Os'].append(i)
                # system = remove_bonds(system, i)

                # el sidd se convierte en sid
                atoms.indices['Sidd'].remove(j)
                atoms.indices['Sid'].append(j)

            total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)

            print(total_charge)

        # si la carga es negative y divisible por 12 saca un oxygeno
        while total_charge < 0 and 10 * total_charge % 12 == 0:

            print('Removing oxygen')

            _, affected = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.select('Si'), '<=1',
                                                 operations_region, remove=operations_region_remove)

            if len(affected) > 0:
                j = np.random.choice(affected)
            else:
                print('Found no atom to remove')
                exit()

            atoms.indices['Ob'].remove(j)

            total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)

            print(total_charge)

        # if positive and divisible by oxygen remove oxygen
        # afterwards a silica will be removed
        # only do once
        if total_charge > 0 and 10 * total_charge % 12 == 0:

            print('Removing oxygen')

            _, affected = find_coordinated_atoms(system, atoms.indices['Ob'], atoms.select('Si'), '<=1',
                                                 operations_region, remove=operations_region_remove)

            j = np.random.choice(affected)

            atoms.indices['Ob'].remove(j)

            total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)

            print(total_charge)

        # si no hay vacuo elimina oxygenos

    total_charge = charge_in_region(system, atoms, inspect_region, inspect_region_remove)

    print('Final system charge %f', total_charge)

    return system, atoms



# ==================================================================================================================== #
# pbc vacuum operations
# ==================================================================================================================== #


def add_vacuum(system, box, atoms, axis, shift=True, shift2=False):

    print('Adding vacuum')

    system = copy.deepcopy(system)

    system.wrap(box)


    for do in range(3):


        #### identify atoms
        # make a list of affected atoms
        # from left (1) to right (2)
        affected1 = []
        affected2 = []

        # make temporary bond list
        temp_bonds = copy.deepcopy(system.bonds)
        remove = []

        # iterate through bonds to identify those that go over pbc
        for j, bond in enumerate(system.bonds):

            if system.xyz[bond[1] - 1, axis] < -3 and system.xyz[bond[2] - 1, axis] > 3:

                affected1.append(bond[1] - 1)
                affected2.append(bond[2] - 1)

                remove.append(j)

            elif system.xyz[bond[1] - 1, axis] > 3 and system.xyz[bond[2] - 1, axis] < -3:

                affected2.append(bond[1] - 1)
                affected1.append(bond[2] - 1)

                remove.append(j)

        if shift:
            # remove bonds j
            for i in reversed(remove):
                temp_bonds = np.delete(temp_bonds, i, 0)


            #### shift isolated atoms
            affected =  affected1 + affected2
            # print(len(np.unique(np.asarray(affected))))

            # iterate through all atoms and if some in affected have no bond shift
            for i in np.unique(np.asarray(affected)):

                bond_counter = 0

                for j in [[1, 2], [2, 1]]:

                    # find if atom index is in column
                    k = np.where(temp_bonds[:, j[0]] == i + 1)[0]  # remember +1 becuase index in python is different than in lammps

                    # if atom index is in column enter if statement
                    if len(k) > 0:

                        # loop through all occurances of atom index in bonds per column
                        for b in k:
                            # if bonded add to bond counter
                            bond_counter += 1

                # print(bond_counter)
                # if it doesnt exist move atom by one box length to the other end
                # atom will be outside of box
                # need to expand box size
                if bond_counter == 0:
                    if system.xyz[i, axis] < 0:
                        system.xyz[i, axis] += box.lengths[axis]
                    elif system.xyz[i, axis] > 0:
                        system.xyz[i, axis] -= box.lengths[axis]

                    if do > 0:
                        print('Isolated atoms should not exist anymore')


            # shift Sib with fewer than 1 Ob nbr
            if shift2:
                if do > 0:
                    # now shift undercoordinated Si atoms
                    # iterate through atom indices we want to remove
                    for i in copy.deepcopy(list(set(affected) & set(atoms.indices['Sib']))):

                        bond_counter = 0

                        for j in [[1, 2], [2, 1]]:

                            # find if atom index is in column
                            k = np.where(temp_bonds[:, j[0]] == i + 1)[0]  # remember +1 becuase index in python is different than in lammps

                            # if atom index is in column enter if statement
                            if len(k) > 0:

                                # loop through all occurances of atom index in bonds per column
                                for b in k:
                                    # if bonded add to bond counter
                                    bond_counter += 1

                        if bond_counter == 0:
                            print('Appears atom index %i had no bond at all' % i)
                            print('It is recomennded to review atom configuration')
                            cont = input('Continue with removing atoms (y/n)')

                            while cont.lower() != 'y':

                                if cont.lower() == 'n':

                                    print('Exiting function without removing atoms')

                                    return None

                                else:
                                    cont = input('Are you sure you want to continue? (y/n): ')

                        # if atom has no bonds rmeove
                        if bond_counter <= 1:
                            if system.xyz[i, axis] < 0:
                                system.xyz[i, axis] += box.lengths[axis]
                            elif system.xyz[i, axis] > 0:
                                system.xyz[i, axis] -= box.lengths[axis]

                            if do > 1:
                                print('Undercoordinated Sib should not exist anymore')

    system.bonds = temp_bonds

    return system, box




# ==================================================================================================================== #
# adding water operations
# ==================================================================================================================== #



def read_h2o(filename):
    '''
    Function that reads h2o.txt file into a system of atoms using groupy

    :param filename:
    :return:
    '''

    file = open(filename, 'r')

    h2o = Gbb()

    types = []
    xyz = []
    charges = []
    bonds = []
    angles = []

    c = 0
    for number, line in enumerate(file.readlines()):
        if line.find('Types') == 0 or \
                line.find('Coords') == 0 or \
                line.find('Charges') == 0 or \
                line.find('Bonds') == 0 or \
                line.find('Angles') == 0:
            c += 1
        elif len(line) > 1:
            if c == 1:
                xyz.append([float(x) for x in line.split()[1:]])
            elif c == 2:
                types.append([int(line.split()[1])])
            elif c == 3:
                charges.append([float(line.split()[1])])
            elif c == 4:
                bonds.append([float(x) for x in line.split()[1:]])
            elif c == 5:
                angles.append([float(x) for x in line.split()[1:]])
                break

    file.close()

    h2o.xyz = np.asarray(xyz)
    h2o.types = np.asarray(types)
    h2o.charges = np.asarray(charges)
    h2o.angles = np.asarray(angles)
    h2o.bonds = np.asarray(bonds)

    return h2o



def add_water(system, box, atoms, region, density):
    '''
    Function to add water in a cubic region

    :param fileh2o:
    :param system:
    :param box:
    :param atoms:
    :param region:
    :param density: in g/cc
    :return:
    '''
    # make such that I define a region and what density I expect within that

    gmol = 18.01528
    NAvogadros = 6.022e23
    rho = density * 1e-24

    water_box = copy.deepcopy(box)
    for i in range(3):
        water_box.mins[i] = min(abs(region[i, 0]), abs(box.mins[i])) * np.sign(region[i, 0])
        water_box.maxs[i] = min(abs(region[i, 1]), abs(box.maxs[i])) * np.sign(region[i, 1])
        water_box.lengths[i] = water_box.maxs[i] - water_box.mins[i]

    print('Water box:')
    print(water_box)

    # check if any atom is inside of water box.
    for xyz in system.xyz:
        if xyz[0] < water_box.maxs[0] and xyz[0] > water_box.mins[0] and \
                xyz[1] < water_box.maxs[1] and xyz[1] > water_box.mins[1] and \
                xyz[2] < water_box.maxs[2] and xyz[2] > water_box.mins[2]:
            print('Region is not empty')
            exit()


    vol = water_box.lengths[0] * water_box.lengths[1] * water_box.lengths[2]

    water = math.ceil(rho * vol / gmol * NAvogadros)

    pts = []
    fac = 10
    while len(pts) < water:
        x = np.linspace(water_box.mins[0], water_box.maxs[0] - 2, int((water_box.lengths[0] - 2) / fac))
        y = np.linspace(water_box.mins[1], water_box.maxs[1] - 2, int((water_box.lengths[1] - 2) / fac))
        z = np.linspace(water_box.mins[2], water_box.maxs[2] - 2, int((water_box.lengths[2] - 2) / fac))[::-1]

        pts = np.zeros((int(len(x)*len(y)*len(z)), 3))
        # print(len(pts), fac, water)
        fac -= 0.25

    c = 0
    for i in range(len(z)):
        for j in range(len(y)):
            for k in range(len(x)):
                pts[c, 0] = x[k]
                pts[c, 1] = y[j]
                pts[c, 2] = z[i]
                c += 1

    np.random.shuffle(pts)

    print('Adding %i water molecules' % water)

    for path in sys.path:
        if path.find('asmbox') > -1:
            app = path[:path.find('asmbox')+6]
    h2o = read_h2o(app+'/h2o.txt')


    xyz = h2o.xyz
    c = 0
    resid = max(system.resids)
    for i in range(0, water * 3, 3):
        c+=1
        resid += 1
        print('Adding water %5d / %5d\r' % ((i+1) / 3, water), end='')
        pos = pts[c-1, :]

        new_id = len(system.types)

        system = insert_atom(system, xyz[0, :] + pos, 1, h2o.charges[0][0], resid+1, 1, [new_id+1, new_id+2])
        system = insert_atom(system, xyz[1, :] + pos, 2, h2o.charges[1][0], resid+1, 1, [])
        system = insert_atom(system, xyz[2, :] + pos, 2, h2o.charges[2][0], resid+1, 1, [])

        atoms.indices['Ow'].append(new_id)
        atoms.indices['Hw'].append(new_id+1)
        atoms.indices['Hw'].append(new_id+2)

    print('')


    return system, atoms



def add_ion(system, box, atoms, ion, num_ions, region, distance):
    '''
    Function to add ions

    :param system:
    :param box:
    :param atoms:
    :param ion:
    :param num_ions:
    :param region:
    :param distance: minimum distance from this ion to other atoms
    :return:
    '''

    system = copy.deepcopy(system)
    atoms = copy.deepcopy(atoms)

    water_box = copy.deepcopy(box)
    for i in range(3):
        water_box.mins[i] = min(abs(region[i, 0]), abs(box.mins[0])) * -1
        water_box.maxs[i] = min(abs(region[i, 1]), abs(box.maxs[1]))
        water_box.lengths[i] = water_box.maxs[i] - water_box.mins[i]

    # print(water_box)

    for i in range(num_ions):
        d = 0
        iter = 0
        while d < distance:
            p = np.random.rand(3) - 0.5
            for j in range(3):
                p[j] *= water_box.lengths[j]
            d = min(distances(p, system.xyz[atoms.all()], box))
            iter += 1
            print('Adding %s ion %i: MC insertion iteration %3i\r' % (ion, i + 1, iter), end='')
            if iter == 200:
                print('Exceeded maximum number of insertion iterations.')
                exit()

        atoms.indices[ion].append(len(system.types))
        system = insert_atom(system, p, atoms.atom_types[ion], 0, 1, 0, [])

    print('')

    return system, atoms



