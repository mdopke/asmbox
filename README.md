# asmbox

Amorphous Silica Manipulation ToolBox

This toolbox is designed for the manipulation of amorphous silica data files.

Amorphous silica is obtained in computational terms from melting and quenching a crystal (beta-cristobalite) block of silica (SiO2).

Once the bulk amorphous silica is created it is often desired to manipulate in such a way until pores exist.

The first step to achieving channels is to convert the block of amorphous silica (here assumed to be Lennard Jones (LJ) atoms as obtained from the BKS (19**) force field) to a bonded file. This means computing all the bonds between Si and O based on distances. Distances between Si and O < 1.8 Angstrom are considered as bonds.

The properties of the amorphous silica are evaluated in terms of radial pair distribution functions and coordination numbers between the Si and O atoms.  

!TODO amorphous silica block characterization needs to be implemented.

After confirming the amorphous silica is good (based on own experience) the channels can be cut out of the bulk

This toolbox supports 2 types of cutes

    1. cubical xlo xhi ylo yhi zlo zhi
    2. spherical xc yc zc r
   
The cut types can be mixed. (Integration is not fully tested)

After performing a cut the system is often left as not charge neutral. To overcome this issue the charge of the system can be balanced. Three approaches are to be followed:

IMPORTANT after cutting the channels the system needs to be relaxed. Bridging and removing atoms can lead to unsustainable configurations. Recommended relaxation time is in the order ot at least 1 ns.
!TODO need to bridge geminals first for more realistic surface
!TODO achieving charge neutrality is currently only supported for cubic cuts

    1. System contains vacuum:
    1.1 not symmetric
       If the system is not symmetric it means that only one wall behind which vacuum is exists
       Then the system will be balanced by removing atoms in the vauum by finding atoms through which the total charge is divisible.
       I.e. total_charge > and total_charge % Si == 0 then remove Si atom in vacuum
       I.e. total_charge < and total_charge % O == 0 then remove O atom in vacuum
       I.e. total_charge % H == 0 atom then remove OH group on surface
    1.2 Symmetric
       If the system is symmetric two walls with vacuum in between throguh a boundary exist. This allows for the charges between the two walls to be balanced by shifting oxygens from one wall to another through the vacuum.
        
      
    2. If the system contains no vacuum:
       The vac flag of balance_system_charge needs to be turned to False. Then the system is balanced in the same way as for vacuum, but instead of removing atoms in the vacuum region the function removes atoms on the surface. The final procedure follows:
       I.e. total_charge > and total_charge % H == 0 then convert OH group to O bulk on surface
       I.e. total_charge < and total_charge % O == 0 then remove O atom on surface
       I.e. total_charge % H == 0 atom then remove OH group on surface
       
There is no guarantee the system will ever be charge neutral. This function is very sensible to cutting location and several cuts might need to be performed to achieve a charge neutral system.

Once the charge neutrality is attained the silanol density is adjusted. The standard setting is 5 silanol/nm2.

After Briding all silanol groups need to be protonated.

Deprotonation using latin hypercube sampling is implemented

Utilities exist to add water and ions in a specified cubic region. Care needs to be taken that there is no overlap with other atoms during insertion      

plot_log plots log files

!TODO make flat AFM tip

!TODO add graphene sheet or think of different solution

!TODO make deprotonation a function of surface charge again

!TODO it is recommended to type in ion parameters individually
